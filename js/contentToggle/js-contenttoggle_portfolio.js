var $elements = $('.js-contentToggle').contentToggle({
  independent: false,
  contentSelector: '+ .js-contentToggle__content',
  toggleOptions : {
    duration : 400
  }
});

// Change summary item color when tab is clicked
$('.accordion__trigger').on('click', function() {
  // Get trigger name
  var sectionName = "#" + $(this).attr('name');


  // If trigger is being closed then open accordion-1
  if( $(sectionName).attr('aria-hidden') === "true" ){
    $('.current').trigger('open');
  }

});
