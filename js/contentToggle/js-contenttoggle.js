var $elements = $('.js-contentToggle').contentToggle({
  independent: false,
  contentSelector: '+ .js-contentToggle__content',
  toggleOptions : {
    duration : 400
  }
});

//TODO When Using single script some redundant code is left.
//     Might cause overhead

// Change summary item color when tab is clicked
$('.accordion__trigger').on('click', function() {
  var summaryName = "#summary_" + $(this).attr('name');
  var sectionName = "#" + $(this).attr('name');

  $('#categories li').removeClass('active');

  // Check if button click closed the tab or opened it
  if( $(sectionName).attr('aria-hidden') === "false" ){
    // Add active to a summary section
    $(summaryName).addClass('active');
  } else {
    $('.current').trigger('open');
    $('#summary_accordion-1').addClass('active');
  }

});


// Open the tab when clicking on the summary item.
$('.js-summary').on('click', function() {

  $('#categories li').removeClass('active');
  $(this).parent().addClass('active');

  var href = $(this).attr('href');
  $(href).trigger('open');
});
